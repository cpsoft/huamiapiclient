# Python Huami API and file client
API client inspired by https://github.com/rolandsz/Mi-Fit-and-Zepp-workout-exporter/

This is not official API. API **is not documented** and can be changed without notice.

# How to setup:
```
pip install git+https://gitlab.com/cpsoft/huamiapiclient.git --upgrade --no-deps --force-reinstall
from huamiapiclient import HuamiApiClient, HuamiFileClient, TYPES_EXERCISES
```

# How to use
## Use with API:
* Find api token and user_id in web console at https://user.huami.com/privacy/index.html#/
* Create client object:

`HAC = HuamiApiClient(token, user_id)`
* Read sport history:
    ```python
        for l in HAC.sport_history(from_track_id):
            print(l)
    ```
* Read body stats:
    ```python
        for l in HAC.weight_records(from_time):
            print(l)
    ```

from_track_id and from_time do **not** affect API queries, Issue #5. Type is [datetime](https://docs.python.org/3/library/datetime.html).

## Use with files:
* Download data files at https://user.huami.com/privacy/index.html#/
* Create client object:

`HAC = HuamiFileClient(weight_records_filename, sport_history_filename)`

weight_records_filename starts with BODY_
sport_history_filename starts with SPORT_


* Read sport history:
    ```python
        for l in HAC.sport_history(from_track_id):
            print(l)
    ```
* Read body stats:
    ```python
        for l in HAC.weight_records(from_time):
            print(l)
    ```
## Data:
* All data timezones is set to UTC.
* `sport_history()` and `weight_records()` are iterables, see code for contents (SportRecord / WeightRecord).
* `TYPES_EXERCISES` contents code, exercise and exercise group (in russian)
