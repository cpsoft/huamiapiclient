from datetime import datetime, UTC
from dataclasses import asdict
import unittest
import json
from pathlib import PurePath
from unittest.mock import patch
from huamiapiclient import HuamiApiClient, HuamiFileClient


# used https://stackoverflow.com/questions/15753390/how-can-i-mock-requests-and-the-response
def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

        def raise_for_status(self):
            pass
    ret = {}
    jsonpath = {"https://api-mifit.huami.com/v1/sport/run/history.json": "history.json",
                "https://api-mifit.huami.com/users/members/-1/weightRecords": "weightRecords.json"
                }

    assert kwargs == {'headers': {'apptoken': '', 'appPlatform': 'web', 'appname': 'com.xiaomi.hm.health'}, 'params': {}, 'timeout': 5}
    with open(PurePath("tests", jsonpath[args[0]]), encoding="utf-8") as f:
        ret = json.loads(f.read())
    return MockResponse(ret, 200)


@patch('requests.get', side_effect=mocked_requests_get)
class TestApiClient(unittest.TestCase):

    def test_basic(self, *args, **keywargs):
        with self.assertRaises(ValueError) as cm:
            _ = HuamiApiClient()
        self.assertEqual(cm.exception.args, ("Must provide API token and user_id", ))
        HAC = HuamiApiClient("", "")
        wrl = list(HAC.weight_records())
        shl = list(HAC.sport_history())
        self.assertEqual(HAC._HuamiApiClient__token, "")
        self.assertEqual(HAC._HuamiApiClient__user_id, "")
        self.assertEqual(len(shl), 2, "2 sport records in API")
        self.assertEqual(len(wrl), 2, "2 weight records in API")
        self.assertEqual(f"{wrl[0]}", "Date 2021-06-18 weight: 76.55 bmi: 28.1", "test weight record string representation")
        self.assertEqual(wrl[0]["date"], datetime(2021, 6, 18, 4, 51, 12, tzinfo=UTC), "test weight __getitem__")
        self.assertEqual(f"{shl[0]}", "Type 52 Start: 2023-11-30 Sport time: 2230.0 End: 2023-11-30 Distance 0.0", "test sport history string representation")
        self.assertEqual(shl[0]["start_time"], datetime(2023, 11, 30, 15, 20, 11, tzinfo=UTC), "test history __getitem__")
        self.assertDictEqual(asdict(shl[0]), {'VO2_max': -1,
                                              'avg_frequency': 0.0,
                                              'avg_heart_rate': 124.0,
                                              'avg_pace': 0.0,
                                              'avg_stride_length': 0,
                                              'calories': 309.0,
                                              'distance': 0.0,
                                              'end_time': datetime(2023, 11, 30, 15, 57, 21, tzinfo=UTC),
                                              'exercise_load': 41,
                                              'max_heart_rate': 151,
                                              'max_pace': 0.0,
                                              'min_heart_rate': 75,
                                              'min_pace': 0.0,
                                              'sport_time': 2230.0,
                                              'start_time': datetime(2023, 11, 30, 15, 20, 11, tzinfo=UTC),
                                              'type': 52})
        self.assertDictEqual(asdict(wrl[0]), {'bmi': 28.1,
                                              'body_score': 36.0,
                                              'bone_mass': 2.8365567,
                                              'date': datetime(2021, 6, 18, 4, 51, 12, tzinfo=UTC),
                                              'fat_rate': 27.174913,
                                              'height': 165.0,
                                              'metabolism': 1594.0,
                                              'muscle_rate': 52.91105,
                                              'body_water_rate': 49.958008,
                                              'protein_ratio': 19.161583,
                                              'visceral_fat': 12.0,
                                              'weight': 76.55})
        track_id = datetime(2023, 12, 10, 23, 59, 59, tzinfo=UTC)
        shl = list(HAC.sport_history(track_id))
        self.assertEqual(len(shl), 0, "0 sport records in filtered API")


class TestFileClient(unittest.TestCase):

    def test_basic_wr(self):
        weight_records_filename = PurePath("tests", "BODY_testasset.csv")
        HFC = HuamiFileClient()
        self.maxDiff = None
        with self.assertRaises(ValueError) as cm:
            f = HFC.weight_records()
            for _ in f:
                pass
        self.assertEqual(cm.exception.args, ("Must provide filename", ))
        HFC = HuamiFileClient(weight_records_filename=weight_records_filename)
        wrl = list(HFC.weight_records())
        self.assertEqual(len(wrl), 13, "13 records for weight at all")
        from_time = datetime(2023, 1, 1, tzinfo=UTC)
        f = list(HFC.weight_records(from_time))
        self.assertEqual(len(f), 6, "6 records from 2023.01.01")
        self.assertEqual(f"{wrl[0]}", "Date 2022-03-11 weight: 71.9 bmi: 26.4", "test weight record string representation")
        self.assertEqual(wrl[0]["date"], datetime(2022, 3, 11, 5, 27, 11, tzinfo=UTC), "test weight __getitem__")
        self.assertDictEqual(asdict(wrl[0]), {'bmi': 26.4,
                                              'body_score': 0,
                                              'body_water_rate': 51.48397,
                                              'bone_mass': 2.744383,
                                              'date': datetime(2022, 3, 11, 5, 27, 11, tzinfo=UTC),
                                              'fat_rate': 24.950481,
                                              'height': 165.0,
                                              'metabolism': 1516.0,
                                              'muscle_rate': 51.21622,
                                              'protein_ratio': 0,
                                              'visceral_fat': 12.0,
                                              'weight': 71.9})

    def test_basic_sporthistory(self):
        sport_history_filename = PurePath("tests", "SPORT_testasset.csv")
        HFC = HuamiFileClient()
        self.maxDiff = None
        with self.assertRaises(ValueError) as cm:
            f = HFC.sport_history()
            for _ in f:
                pass
        self.assertEqual(cm.exception.args, ("Must provide filename", ))
        HFC = HuamiFileClient(sport_history_filename=sport_history_filename)
        f = list(HFC.sport_history())
        self.assertEqual(len(f), 14, "14 records for sport at all")
        self.assertEqual(f"{f[0]}", "Type 52 Start: 2023-11-12 Sport time: 3712.0 End: 2023-11-12 Distance 0.0", "test sport history string representation")
        self.assertEqual(f[0]["start_time"], datetime(2023, 11, 12, 12, 48, 1, tzinfo=UTC), "test history __getitem__")
        track_id = datetime(2023, 11, 10, 23, 59, 59, tzinfo=UTC)
        f = list(HFC.sport_history(track_id))
        self.assertEqual(len(f), 3, "3 records from 2023.11.10")
        self.assertDictEqual(asdict(f[0]), {'VO2_max': 0,
                                            'avg_frequency': 0,
                                            'avg_heart_rate': 0,
                                            'avg_pace': 0.0,
                                            'avg_stride_length': 0,
                                            'calories': 469.0,
                                            'distance': 0.0,
                                            'end_time': datetime(2023, 11, 12, 12, 48, 1, tzinfo=UTC),
                                            'exercise_load': 0,
                                            'max_heart_rate': 0,
                                            'max_pace': 0.0,
                                            'min_heart_rate': 0,
                                            'min_pace': 0.0,
                                            'sport_time': 3712.0,
                                            'start_time': datetime(2023, 11, 12, 12, 48, 1, tzinfo=UTC),
                                            'type': '52'})


if __name__ == '__main__':
    unittest.main()
