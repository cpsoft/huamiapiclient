"""Utility functions for api client"""
from typing import Any, Dict
from urllib.parse import urljoin
import requests


def _settings(param: str) -> str:
    """Constant settings"""
    ret = {
        "APP_NAME": "com.xiaomi.hm.health",
        "APP_PLATFORM": "web",
        "BASE_URL": "https://api-mifit.huami.com"
    }
    return ret[param]


def getnullorfloat(line: Dict[str, str], paramname: str) -> Any:
    """Return null or float line[paramname]"""
    ret = line.get(paramname, 0)
    if ret == "null":
        return "NULL"
    return float(ret)


def do_request(endpoint: str, token: str, params: Dict[str, str | int]) -> Any:
    """do request to API"""
    response = requests.get(
        urljoin(_settings("BASE_URL"), endpoint),
        headers={
            "apptoken": token,
            "appPlatform": _settings("APP_PLATFORM"),
            "appname": _settings("APP_NAME"),
        },
        params=params,
        timeout=5
    )
    response.raise_for_status()
    return response.json()


TYPES_EXERCISES = [{"type": 1, "exercise": "Бег на открытом воздухе", "exercise_group": "Бег"},
                   {"type": 1001, "exercise": "Бег на открытом воздухе", "exercise_group": "Бег"},
                   {"type": 8, "exercise": "Бег на дорожке", "exercise_group": "Бег"},
                   {"type": 9, "exercise": "Велосипед", "exercise_group": "Велосипед"},
                   {"type": 1009, "exercise": "Велосипед", "exercise_group": "Велосипед"},
                   {"type": 10, "exercise": "Велотренажер", "exercise_group": "Велосипед"},
                   {"type": 143, "exercise": "Велотренажер", "exercise_group": "Велосипед"},
                   {"type": 12, "exercise": "Эллипсоид", "exercise_group": "Эллипсоид"},
                   {"type": 23, "exercise": "Гребной тренажер", "exercise_group": "Гребной тренажер"},
                   {"type": 14, "exercise": "Плавание в бассейне", "exercise_group": "Плавание"},
                   {"type": 15, "exercise": "Плавание в открытой воде", "exercise_group": "Плавание"},
                   {"type": 1015, "exercise": "Плавание в открытой воде", "exercise_group": "Плавание"},
                   {"type": 66, "exercise": "Гребля на лодке", "exercise_group": "Гребной спорт"},
                   {"type": 68, "exercise": "Сапсёрфинг", "exercise_group": "Гребной спорт"},
                   {"type": 52, "exercise": "Силовой тренинг", "exercise_group": "Силовой тренинг"},
                   {"type": 6, "exercise": "Ходьба", "exercise_group": "Ходьба"},
                   {"type": 24, "exercise": "Крытый фитнес", "exercise_group": "Крытый фитнес"},
                   {"type": 44, "exercise": "Катание на коньках", "exercise_group": "Катание на коньках"},
                   {"type": 21, "exercise": "Прыжки на скакалке", "exercise_group": "Прыжки на скакалке"},
                   {"type": 130, "exercise": "Кросс-тренировка", "exercise_group": "Кросс-тренировка"},
                   {"type": 16, "exercise": "Свободная тренировка", "exercise_group": "Свободная тренировка"}
                   ]
