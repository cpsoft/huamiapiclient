"""Interfaces for used classes"""
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from datetime import datetime
from typing import Optional, Iterator, Any


@dataclass
class SportRecordData:
    """SportRecord dataclass"""
    start_time: datetime = field(init=False)
    end_time: datetime = field(init=False)
    sport_time: float = field(init=False)
    type: str = field(init=False)
    max_heart_rate: int = field(init=False)
    min_heart_rate: int = field(init=False)
    max_pace: float = field(init=False)
    min_pace: float = field(init=False)
    avg_pace: float = field(init=False)
    avg_heart_rate: float = field(init=False)
    distance: float = field(init=False)
    avg_frequency: float = field(init=False)
    avg_stride_length: int = field(init=False)
    calories: float = field(init=False)
    exercise_load: int = field(init=False)
    VO2_max: int = field(init=False)  # pylint: disable=invalid-name

    def __repr__(self) -> str:
        return f"Type {self.type} Start: {self.start_time.date()} Sport time: {self.sport_time} End: {self.end_time.date()} Distance {self.distance}"

    def __getitem__(self, item: str) -> int | float:
        return getattr(self, item, 0)


@dataclass
class WeightRecordData:
    """WeightRecord dataclass"""
    date: datetime = field(init=False)
    weight: float = field(init=False)
    height: float = field(init=False)
    bmi: float = field(init=False)
    fat_rate: float = field(init=False)
    body_water_rate: float = field(init=False)
    bone_mass: float = field(init=False)
    metabolism: float = field(init=False)
    muscle_rate: float = field(init=False)
    visceral_fat: float = field(init=False)
    body_score: float = 0
    protein_ratio: float = 0

    def __repr__(self) -> str:
        return f"Date {self.date.date()} weight: {self.weight} bmi: {self.bmi}"

    def __getitem__(self, item: str) -> Any:
        return getattr(self, item)


class APIClient(ABC):
    """Abstract client class."""
    @abstractmethod
    def sport_history(self, from_track_id: Optional[datetime] = None) -> Iterator[SportRecordData]:
        """sport history interface.
        from_track_id is a name from API, actually a UNIX format datetime"""

    @abstractmethod
    def weight_records(self, from_time: Optional[datetime] = None) -> Iterator[WeightRecordData]:
        """weight records interface"""
