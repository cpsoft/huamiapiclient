"""Client for Amazfit huami API"""

from .huamiapiclient import HuamiApiClient, HuamiFileClient
from .util import TYPES_EXERCISES
__all__ = ['HuamiApiClient', 'HuamiFileClient', 'TYPES_EXERCISES']
