"""Client for Amazfit huami API"""
from typing import Optional, Iterator, Any
from datetime import datetime, UTC
import csv
from .util import getnullorfloat, do_request
from .interface import APIClient, SportRecordData, WeightRecordData


class SportRecord(SportRecordData):
    """Record for sport data"""
    def __init__(self, line: Any):
        super().__init__()
        if "trackid" in line:  # line from API
            self.start_time = datetime.fromtimestamp(int(line.get("trackid")), UTC)
            self.end_time = datetime.fromtimestamp(int(line.get("end_time")), UTC)
            self.sport_time = float(line.get("run_time"))  # seconds
            self.type = line.get("type")
            self.max_heart_rate = int(line.get("max_heart_rate"))
            self.min_heart_rate = int(line.get("min_heart_rate"))
            self.max_pace = float(line.get("max_pace"))
            self.min_pace = float(line.get("min_pace"))
            self.avg_pace = float(line.get("avg_pace"))
            self.avg_heart_rate = float(line.get("avg_heart_rate"))
            self.distance = float(line.get("dis"))  # meters
            self.avg_frequency = float(line.get("avg_frequency"))
            self.avg_stride_length = int(line.get("avg_stride_length"))
            self.calories = float(line.get("calorie"))  # kcal
            self.exercise_load = int(line.get("exercise_load"))
            self.VO2_max = int(line.get("VO2_max"))
        else:  # line from csv file
            self.start_time = datetime.fromisoformat(line["startTime"])
            self.end_time = datetime.fromisoformat(line["startTime"])
            self.sport_time = float(line.get("sportTime(s)"))
            self.type = line.get("type")
            self.max_heart_rate = 0
            self.min_heart_rate = 0
            self.max_pace = float(line.get("maxPace(/meter)"))
            self.min_pace = float(line.get("minPace(/meter)"))
            self.avg_pace = float(line.get("avgPace(/meter)"))
            self.avg_heart_rate = 0.0
            self.distance = float(line.get("distance(m)"))  # meters
            self.avg_frequency = 0.0
            self.avg_stride_length = 0
            self.calories = float(line.get("calories(kcal)"))  # kcal
            self.exercise_load = 0
            self.VO2_max = 0


class WeightRecord(WeightRecordData):
    """Record for weight data"""
    def __init__(self, line: Any):
        super().__init__()
        if "generatedTime" in line:  # line from API
            self.date = datetime.fromtimestamp(int(line["generatedTime"]), UTC)
            self.weight = float(line.get("summary").get("weight"))
            self.height = float(line.get("summary").get("height", 0))
            self.bmi = float(line.get("summary").get("bmi", 0))
            self.body_score = float(line.get("summary").get("bodyScore", 0))
            self.fat_rate = float(line.get("summary").get("fatRate", 0))
            self.body_water_rate = float(line.get("summary").get("bodyWaterRate", 0))
            self.bone_mass = float(line.get("summary").get("boneMass", 0))
            self.metabolism = float(line.get("summary").get("metabolism", 0))
            self.muscle_rate = float(line.get("summary").get("muscleRate", 0))
            self.visceral_fat = float(line.get("summary").get("visceralFat", 0))
            self.protein_ratio = float(line.get("summary").get("proteinRatio", 0))
        else:  # line from csv file
            self.date = datetime.fromisoformat(line["time"])
            self.weight = float(line.get("weight"))
            self.height = float(line.get("height"))
            self.bmi = float(line.get("bmi"))

            self.fat_rate = getnullorfloat(line, "fatRate")
            self.body_water_rate = getnullorfloat(line, "bodyWaterRate")
            self.bone_mass = getnullorfloat(line, "boneMass")
            self.metabolism = getnullorfloat(line, "metabolism")
            self.muscle_rate = getnullorfloat(line, "muscleRate")
            self.visceral_fat = getnullorfloat(line, "visceralFat")


class HuamiApiClient(APIClient):
    """Huami API client"""
    def __init__(self, token: Optional[str] = None, user_id: Optional[str] = None):
        if token is None or user_id is None:
            raise ValueError("Must provide API token and user_id")
        self.__token = token
        self.__user_id = user_id

    def sport_history(self, from_track_id: Optional[datetime] = None) -> Iterator[SportRecordData]:
        "Retrieve sport history"
        response = do_request(
            endpoint="/v1/sport/run/history.json",
            token=self.__token,
            params={}  # {"trackid": from_track_id} if from_track_id is not None else {},  # trackid param not working, Issue#15
        )
        for _sr in map(SportRecord, response["data"]["summary"]):
            if from_track_id is not None:
                if _sr.start_time <= from_track_id:
                    continue
            yield _sr

    def weight_records(self, from_time: Optional[datetime] = None) -> Iterator[WeightRecordData]:
        """Retrieve weight history.
        param from_time is type datetime.datetime or None"""
        # https://gist.github.com/rolandsz/7323bb8ddf819998b9642ae7a6dd9f80
        # from_time = datetime.datetime(2000, 1, 1)
        # from_time = int(datetime.datetime.timestamp(from_time))
        # Параметр from_time работает следующим образом: Выдаются последние (поздние)
        # строки количеством не больше 100 и с датой не менее from_time.
        # Историю глубже 100 строк не выдает(!)
        response = do_request(
            endpoint=f"users/{self.__user_id}/members/-1/weightRecords",
            token=self.__token,
            params={"fromTime": int(datetime.timestamp(from_time))} if from_time is not None else {},
        )

        yield from map(WeightRecord, response["items"])


class HuamiFileClient(APIClient):
    """Huami file client"""
    def __init__(self, weight_records_filename: Optional[str] = None, sport_history_filename: Optional[str] = None):
        self.__weight_records_filename = weight_records_filename
        self.__sport_history_filename = sport_history_filename

    def weight_records(self, from_time: Optional[datetime] = None) -> Iterator[WeightRecordData]:
        """returns weight records from csv file, starting from from_time"""
        if self.__weight_records_filename is None:
            raise ValueError("Must provide filename")
        with open(self.__weight_records_filename, encoding="utf-8-sig") as csvfile:
            spamreader = csv.DictReader(csvfile, delimiter=',')
            for _wr in map(WeightRecord, spamreader):
                if from_time is not None:
                    if _wr.date <= from_time:
                        continue
                yield _wr

    def sport_history(self, from_track_id: Optional[datetime] = None) -> Iterator[SportRecordData]:
        """returns sport history from csv file"""
        if self.__sport_history_filename is None:
            raise ValueError("Must provide filename")
        with open(self.__sport_history_filename, encoding="utf-8-sig") as csvfile:
            spamreader = csv.DictReader(csvfile, delimiter=',')
            for _sr in map(SportRecord, spamreader):
                if from_track_id is not None:
                    if _sr.start_time <= from_track_id:
                        continue
                yield _sr
